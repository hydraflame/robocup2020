#ifndef HC05_h
#define HC05_h

#include <Arduino.h>

enum class Device{
  CV, WHEEL, BG
};

class HC05{
  public:
    void send(String s, Device dv);
    String read();
    HC05(char type);
  private:
    char type;
    void wait();
    String cvs = "#CV#";
    String wls = "#WL#";
    String bgs = "#BG#";
    String end = "#END#";
};

#endif
