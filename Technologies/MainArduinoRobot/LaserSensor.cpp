#include "LaserSensor.h"

float LaserSensor::getDistance(){
  VL53L0X_RangingMeasurementData_t measure;

  sen.rangingTest(&measure, false);
  return measure.RangeMilliMeter;
}

void LaserSensor::init(){
  sen.begin();
}

bool LaserSensor::isBegin(){
  return sen.begin();
}
