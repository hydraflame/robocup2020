#include "HC05.h"
#include "Defs.h"
HC05::HC05(char type){
  this->type = type;
}

void HC05::send(String s, Device dv){
  if(this->type == transmitterBT){
    String msg = "";
    switch (dv) {
      CV:
        msg = this->cvs;
        msg += s;
        msg += this->end;
        Serial2.println(msg);
        break;
      WHEEL:
        msg = this->wls;
        msg += s;
        msg += this->end;
        Serial2.println(msg);
        break;
      BG:
        msg = this->bgs;
        msg += s;
        msg += this->end;
        Serial2.println(msg);
        break;
    }
  }
}

void HC05::wait(){
  while(!Serial2.available());
}

String HC05::read(){
  if(this->type == receiverBT){
    this->wait();
    String s = Serial2.readStringUntil('\n');
    return s;
  }else{
    return "";
  }
}
