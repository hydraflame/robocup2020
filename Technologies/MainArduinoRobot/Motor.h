#ifndef Motor_h
#define Motor_h

class Motor{
  private:
    char decode_pin;
    int vel = 220;
  public:
    void init();
    void goForward();
    void goBack();
    void setVel();
    void stopM();
    Motor(char decode_pin);
};

#endif
