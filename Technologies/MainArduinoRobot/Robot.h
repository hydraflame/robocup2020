#ifndef Robot_h
#define Robot_h

#include <Arduino.h>
#include <PololuMaestro.h>
#include "Defs.h"
#include "Motor.h"
#include "ServoMotor.h"
#include "LaserSensor.h"
#include "RF433.h"
#include "HC05.h"

struct Motors{
  //sx motors
  Motor sxMotor = Motor(sxDec);
  //dx motors
  Motor dxMotor = Motor(dxDec);

  void begin(){
    sxMotor.init();
    sxMotor.setVel();
    dxMotor.init();
    dxMotor.setVel();
  }

  void goForward(){
    //stop motors
    sxMotor.stopM();
    dxMotor.stopM();
    //go forward motors
    sxMotor.goForward();
    dxMotor.goForward();
  }

  void goBack(){
    //stop motors
    sxMotor.stopM();
    dxMotor.stopM();
    //go back motors
    sxMotor.goBack();
    dxMotor.goBack();
  }

  void stop(){
    sxMotor.stopM();
    dxMotor.stopM();
  }

  void turnRight90(){
    //stop motors
    sxMotor.stopM();
    dxMotor.stopM();
    //go forward sx motor
    sxMotor.goForward();
    //go back dx motor
    dxMotor.goBack();
    delay(1500); //indicativo
    //stop motors
    sxMotor.stopM();
    dxMotor.stopM();
  }

  void turnLeft90(){
    //stop motors
    sxMotor.stopM();
    dxMotor.stopM();
    //go forward dx motor
    dxMotor.goForward();
    //go back sx motor
    sxMotor.goBack();
    delay(1500); //indicativo
    //stop motors
    sxMotor.stopM();
    dxMotor.stopM();
  }
};

struct Moviments{
  ServoMotor dxShoulder = ServoMotor(shoulderDx);
  ServoMotor sxShoulder = ServoMotor(shoulderSx);
  ServoMotor dxArm = ServoMotor(armDx);
  ServoMotor sxArm = ServoMotor(armSx);
  ServoMotor neckHigh = ServoMotor(highNeck);
  ServoMotor neckLow = ServoMotor(lowNeck);
  ServoMotor dxElbo = ServoMotor(elboDx);
  ServoMotor sxElbo = ServoMotor(elboSx);

  void go(){
    Serial1.begin(9600);
  }
  //shouders
  void raiseDxShoulder(){
    dxShoulder.getUpFull();
  }

  void raiseSxShoulder(){
    sxShoulder.getUpFull();
  }

  //arms
  void raiseDxArm(){
    dxArm.getUpFull();
  }

  void raiseSxArm(){
    sxArm.getUpFull();
  }

  //neck
  void raiseNeckLow(){
    neckHigh.getUpFull();
  }

  void raiseNeckHigh(){
    neckLow.getUpFull();
  }

  //elbos
  void raiseDxElbo(){
    dxElbo.getUpFull();
  }

  void raiseSxElbo(){
    sxElbo.getUpFull();
  }

  //other
  //shouders
  void lowerDxShoulder(){
    dxShoulder.getDownFull();
  }

  void lowerSxShoulder(){
    sxShoulder.getDownFull();
  }

  //arms
  void lowerDxArm(){
    dxArm.getDownFull();
  }

  void lowerSxArm(){
    sxArm.getDownFull();
  }

  //neck
  void lowerNeckLow(){
    neckHigh.getDownFull();
  }

  void lowerNeckHigh(){
    neckLow.getDownFull();
  }

  //elbos
  void lowerSxElbo(){
    sxElbo.getDownFull();
  }

  void lowerDxElbo(){
    dxElbo.getDownFull();
  }
};

struct BTS{
  HC05 txBT = HC05(transmitterBT);
  HC05 rxBT = HC05(receiverBT);

  void init(){
    Serial2.begin(115200);
  }

  void sendInfo(String s, Device dv){
    txBT.send(s, dv);
  }
  
  String readInfo(){
    return rxBT.read();
  }
};

class Robot{
  public:
    //begin method
    void init();
    //motors method
    void forward();
    void back();
    void turnLeft();
    void turnRight();
    void stop();

    //servos method
    void raiseLeftArm();
    void raiseRightArm();
    void lowerLeftArm();
    void lowerRightArm();
    void sayYes();
    void sayNo();

    //distance sensor method
    float distance();
    //RF433 sender
    boolean sendRF(String msg);
    //HC05 sender
    void sendBT(String msg, Device dv);
    //HC05 receiver
    String readBT();
    Motors motors;
  private:
    RF433 tx = RF433(txType, txPin);
    BTS bts;
    LaserSensor sensor;
    Moviments moviments;
    bool isDistanceBegin();
    
};

#endif
