#ifndef RF433_h
#define RF433_h

#include <Arduino.h>
#include <VirtualWire.h>

class RF433{
  private:
    char type;
    int pin;
    char receiverBuf[VW_MAX_MESSAGE_LEN];
    char transmitterBuf[VW_MAX_MESSAGE_LEN];
    uint8_t buflen = VW_MAX_MESSAGE_LEN;
    String in = "#begin#";
    String out = "#end#";
    String toStr();
  public:
    RF433(char type, int pin);
    void init();
    boolean write(String s);
    String read();
};

#endif
