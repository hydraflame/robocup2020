#include "Motor.h"
#include "Defs.h"
#include <Arduino.h>

//  MOTOR A | MOTOR B | OUTPUT
//    LOW   |   LOW   |   fermo
//    LOW   |   HIGH  |   indietro
//    HIGH  |   LOW   |   avanti
//    HIGH  |   HIGH  |   fermo

Motor::Motor(char decode_pin){
  this->decode_pin = decode_pin;
}

void Motor::init(){
  Serial.begin(9600);
  switch(decode_pin){
    case sxDec:
      Serial.println("SX");
      pinMode(firstSxPin, OUTPUT);
      pinMode(lastSxPin, OUTPUT);
      pinMode(sxSpeed, OUTPUT);
      break;

    case dxDec:
      Serial.println("DX");
      pinMode(firstDxPin, OUTPUT);
      pinMode(lastDxPin, OUTPUT);
      pinMode(dxSpeed, OUTPUT);
      break;
  }
}

void Motor::setVel(){
  switch (decode_pin) {
    case sxDec:
      analogWrite(sxSpeed, vel);
      break;

    case dxDec:
      analogWrite(dxSpeed, vel);
      break;
  }
}

void Motor::goForward(){
  Serial.begin(9600);
  switch (this->decode_pin) {
    case sxDec:
      Serial.println("HereSX");
      digitalWrite(firstSxPin, HIGH);
      digitalWrite(lastSxPin, LOW);
      break;

    case dxDec:
      digitalWrite(firstDxPin, HIGH);
      digitalWrite(lastDxPin, LOW);
      Serial.println(digitalRead(firstDxPin));
      Serial.println(digitalRead(lastDxPin));
      break;
  }
}

void Motor::goBack(){
  switch (decode_pin) {
    case sxDec:
      digitalWrite(firstSxPin, LOW);
      digitalWrite(lastSxPin, HIGH);
      break;

    case dxDec:
      digitalWrite(firstDxPin, LOW);
      digitalWrite(lastDxPin, HIGH);
      break;
  }
}

void Motor::stopM(){
  switch (decode_pin) {
    case sxDec:
      digitalWrite(firstSxPin, LOW);
      digitalWrite(lastSxPin, LOW);
      break;

    case dxDec:
      digitalWrite(firstDxPin, LOW);
      digitalWrite(lastDxPin, LOW);
      break;
  }
}
