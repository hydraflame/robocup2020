#include "ServoMotor.h"
#include "Defs.h"
#include <Arduino.h>
#include "Robot.h"

ServoMotor::ServoMotor(int idfc){
  this->idfc = idfc;
}

void ServoMotor::set(int idfc){
  this->idfc = idfc;
}

void ServoMotor::getUpFull(){
  MiniMaestro myMaestro(Serial1);

  switch(idfc){
    case shoulderDx:
      myMaestro.setTarget(shoulderDx, 7000);
      break;

    case shoulderSx:
      myMaestro.setTarget(shoulderSx, 6000);
      break;

    case armDx:
      myMaestro.setTarget(armDx, 8000);
      break;

    case armSx:
      myMaestro.setTarget(armSx, 8000);
      break;

    case lowNeck:
      myMaestro.setTarget(lowNeck, 8000);
      break;

    case highNeck:
      myMaestro.setTarget(highNeck, 8000);
      break;

    case elboDx:
      myMaestro.setTarget(elboDx, 8000);
      break;

    case elboSx:
      myMaestro.setTarget(elboSx, 8000);
      break;
  }
}

void ServoMotor::getDownFull(){
  MiniMaestro myMaestro(Serial1);

  switch(idfc){
    case shoulderDx:
      myMaestro.setTarget(shoulderDx, 4000);
      break;

    case shoulderSx:
      myMaestro.setTarget(shoulderSx, 4000);
      break;

    case armDx:
      myMaestro.setTarget(armDx, 4000);
      break;

    case armSx:
      myMaestro.setTarget(armSx, 4000);
      break;

    case lowNeck:
      myMaestro.setTarget(lowNeck, 4000);
      break;

    case highNeck:
      myMaestro.setTarget(highNeck, 4000);
      break;

    case elboDx:
      myMaestro.setTarget(elboDx, 4000);
      break;

    case elboSx:
      myMaestro.setTarget(elboSx, 4000);
      break;
  }
}

void ServoMotor::move(int pos){
  MiniMaestro myMaestro(Serial1);

  switch(idfc){
    case shoulderDx:
      myMaestro.setTarget(shoulderDx, pos);
      break;

    case shoulderSx:
      myMaestro.setTarget(shoulderSx, pos);
      break;

    case armDx:
      myMaestro.setTarget(armDx, pos);
      break;

    case armSx:
      myMaestro.setTarget(armSx, pos);
      break;

    case lowNeck:
      myMaestro.setTarget(lowNeck, pos);
      break;

    case highNeck:
      myMaestro.setTarget(highNeck, pos);
      break;

    case elboDx:
      myMaestro.setTarget(elboDx, pos);
      break;

    case elboSx:
      myMaestro.setTarget(elboSx, pos);
      break;
  }
}
