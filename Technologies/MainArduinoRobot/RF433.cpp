#include "RF433.h"
#include "Defs.h"

RF433::RF433(char type, int pin){
  this->type = type;
  this->pin = pin;
}

void RF433::init(){
  if(this->type == rxType){
    vw_set_rx_pin(this->pin);
    vw_setup(baudRate);
    vw_rx_start();
  }
  else if(this->type == txType){
    pinMode(this->pin, OUTPUT);
    vw_set_tx_pin(this->pin);
    vw_setup(baudRate);
  }
}

boolean RF433::write(String str){
  if(this->type == txType){
    String msg = "";
    msg += in;
    msg += str;
    msg += out;
    msg.toCharArray(transmitterBuf, 16);
    vw_send(transmitterBuf, 16);
  }
    vw_wait_tx();
    return true; //message sending correctly
  }

String RF433::read(){
  if(this->type == rxType){
    vw_wait_rx();
    if(vw_get_message(this->receiverBuf, &this->buflen)){
      return this->toStr();
    }
  }
}

String RF433::toStr(){
  String ret = "";
  for(int i = 0; i < this->buflen; i++){
    if(receiverBuf[i] != '\0'){
      ret += receiverBuf[i];
    }
  }
  return ret;
}
