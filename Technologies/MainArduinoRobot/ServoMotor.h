#ifndef ServoMotor_h
#define ServoMotor_h

#include <PololuMaestro.h>
#include <Arduino.h>

class ServoMotor{
  public:
    ServoMotor(int idfc);
    void getUpFull();
    void getDownFull();
    void move(int pos);
    void set(int idfc);
  private:
    int idfc; //identificator
};

#endif
