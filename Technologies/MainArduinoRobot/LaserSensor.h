#ifndef LaserSensor_h
#define LaserSensor_h

#include "Adafruit_VL53L0X.h"

class LaserSensor{
  public:
    float getDistance();
    void init();
    bool isBegin();
    Adafruit_VL53L0X sen = Adafruit_VL53L0X();
};

#endif
