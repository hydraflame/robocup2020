#ifndef defs_h
#define defs_h

//motors pin
#define firstSxPin 2
#define lastSxPin 3
const int sxSpeed = 9; //PWM

#define firstDxPin 4
#define lastDxPin 5
const int dxSpeed = 10; //PWM

//interrupt pin
#define virtualPin 7

//different motor
const char sxDec = 'a';
const char dxDec = 'd';

//servo motors
const int shoulderDx = 0;
const int shoulderSx = 1;
const int armDx = 2;
const int armSx = 3;
const int lowNeck = 4;
const int highNeck = 5;
const int elboDx = 6;
const int elboSx = 7;

//RF433
const char txType = 't';
const char rxType = 'r';
const int txPin = 6;
const int rxPin = 11;
#define baudRate 2000

//HC05
const char transmitterBT = 'x';
const char receiverBT = 'y';

#endif
