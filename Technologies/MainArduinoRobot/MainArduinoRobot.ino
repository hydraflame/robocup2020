#include "Robot.h"
#include "HC05.h"
Robot robot;

void setup() {
  robot.init();
  Serial.println("[DEBUG] Robot initialize...");
  Serial.begin(9600);

  pinMode(virtualPin, INPUT);
  digitalWrite(virtualPin, LOW);
  //attachInterrupt(virtualPin, interruptLoop, RISING);
}

void loop() {
  // put your main code here, to run repeatedly:
  robot.motors.dxMotor.goForward();
  while(true){}
}

void yesno(){
  robot.sayYes();
  delay(1000);
  robot.sayNo();
  delay(1000);
}
//main loop
void robotMainSoftware(){
  robot.forward();
  while(robot.distance() >= 25.0f) {
    armsMove();
  }
  robot.sendBT("hello", Device::CV);
  String cvRep = robot.readBT(); // wait the answer
  respControl(cvRep);
  robot.sendBT("arrive", Device::CV);
  cvRep = robot.readBT();
  respControl(cvRep);

  while(robot.distance() >= 25.0f) {
    armsMove();
  }
  robot.stop();
}

void respControl(String s){
  if(s.equals("go")){ //if it read "go", go to the station
    robot.turnLeft();
    robot.forward();

    while(robot.distance() >= 25.0f) {
      armsMove();
    }

    robot.turnLeft();
    robot.turnLeft();
  }
  else if(s.equals("school")){
    robot.turnLeft();
    robot.forward();
  }
}

void interruptLoop(){
  robot.turnRight();
  delay(500);
  robot.turnRight();
  delay(500);
  robot.turnRight();
  delay(500);

  robot.forward(); //robot go into the school
}

void interruptTest(){
  robot.sayYes();
  robot.sayYes(); //test
}

void armsMove(){ // servomotors moviments
    robot.raiseLeftArm();
    robot.raiseRightArm();
    delay(1000);
    robot.lowerLeftArm();
    robot.lowerRightArm();
    delay(1000);
}
