#include "Robot.h"

void Robot::init(){
  motors.begin();
  sensor.init();
  isDistanceBegin();
  //servo motors begin
  //serial begin
  moviments.go();
  tx.init();
}

//RF433
boolean Robot::sendRF(String msg){
  return tx.write(msg);
}

//BT
void Robot::sendBT(String msg, Device dv){
  bts.sendInfo(msg, dv);
}

String Robot::readBT(){
  return bts.readInfo();
}

// adapting the methods of the Motors structure
void Robot::forward(){
  motors.goForward();
}

void Robot::back(){
  motors.goBack();
}

void Robot::stop(){
  motors.stop();
}
void Robot::turnLeft(){
  motors.turnLeft90();
}

void Robot::sayYes(){
  moviments.raiseNeckHigh();
  delay(500);
  moviments.lowerNeckHigh();
  delay(500);
  moviments.raiseNeckHigh();
  delay(500);
  moviments.lowerNeckHigh();
}

void Robot::sayNo(){
  moviments.raiseNeckLow();
  delay(500);
  moviments.lowerNeckLow();
  delay(500);
  moviments.raiseNeckLow();
  delay(500);
  moviments.lowerNeckLow();
}

void Robot::turnRight(){
  motors.turnRight90();
}

float Robot::distance(){
  return sensor.getDistance();
}

void Robot::raiseLeftArm(){
  moviments.raiseSxShoulder();
  moviments.raiseSxArm();
  moviments.raiseSxElbo();
}

void Robot::raiseRightArm(){
  moviments.raiseDxShoulder();
  moviments.raiseDxArm();
  moviments.lowerDxElbo();
}

void Robot::lowerLeftArm(){
  moviments.raiseSxShoulder();
  moviments.lowerSxArm();
  moviments.raiseSxElbo();
}

void Robot::lowerRightArm(){
  moviments.raiseDxShoulder();
  moviments.lowerDxArm();
  moviments.lowerDxElbo();
}

bool Robot::isDistanceBegin(){
  return sensor.isBegin();
}
