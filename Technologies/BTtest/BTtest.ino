#include <SoftwareSerial.h>

SoftwareSerial BTSerial(5, 6); // RX | TX

void setup()
{
  pinMode(9, OUTPUT);  // questo pin è connesso al relativo pin 34 (pin KEY) del HC-05
  // che portato a HIGH permette di passare alla modalità AT
  digitalWrite(9, HIGH);
  Serial.begin(9600);
  Serial.println("Inserire i comandi AT:");
  BTSerial.begin(38400);  // Velocità di default del modulo HC-05
}

void loop()
{
  // Continua a leggere da HC-05 e invia Serial Monitor Arduino
  if (BTSerial.available())
    Serial.write(BTSerial.read());

  // Continua a leggere da Arduino Serial Monitor e invia a HC-05
  if (Serial.available())
    BTSerial.write(Serial.read());
}
