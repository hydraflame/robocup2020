#include "Defs.h"
#include "Motor.h"
#include "HC05.h";

const HC05 rx = HC05(receiverBT);
Motor motor = Motor(dxDec);

void setup() {
  // put your setup code here, to run once:
  motor.init();
  Serial.begin(9600);
}

void loop() {
  motor.goForward();
}

void test(){
  //rx.wait();
  //String resp = rx.read();
  String resp = "#WL#start";
  if(resp.charAt(1) == 'W' && resp.charAt(2) == 'L'){
    resp.replace("#WL#", "");
    if(resp.equals("start")){
      motor.goForward();
    }
  }
  while(true);
}
