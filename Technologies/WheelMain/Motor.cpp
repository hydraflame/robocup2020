#include "Motor.h"
#include "Defs.h"
#include <Arduino.h>

//  MOTOR A | MOTOR B | OUTPUT
//    LOW   |   LOW   |   fermo
//    LOW   |   HIGH  |   indietro
//    HIGH  |   LOW   |   avanti
//    HIGH  |   HIGH  |   fermo

Motor::Motor(char decode_pin){
  this->decode_pin = decode_pin;
}

void Motor::init(){
  switch(decode_pin){
    case sxDec:
      pinMode(firstSxPin, OUTPUT);
      pinMode(lastSxPin, OUTPUT);
      pinMode(sxSpeed, OUTPUT);
      break;

    case dxDec:
      pinMode(firstDxPin, OUTPUT);
      pinMode(lastDxPin, OUTPUT);
      pinMode(dxSpeed, OUTPUT);
      break;
  }
}

void Motor::setVel(){
  switch (decode_pin) {
    case sxDec:
      analogWrite(sxSpeed, speed);
      break;

    case dxDec:
      analogWrite(dxSpeed, speed);
      break;
  }
}

void Motor::goForward(){
  switch (decode_pin) {
    case sxDec:
      Serial.print("I'm here");
      digitalWrite(firstSxPin, HIGH);
      digitalWrite(lastSxPin, LOW);
      break;

    case dxDec:
      digitalWrite(firstDxPin, HIGH);
      digitalWrite(lastDxPin, LOW);
      break;
  }
}

void Motor::goBack(){
  switch (decode_pin) {
    case sxDec:
      digitalWrite(firstSxPin, LOW);
      digitalWrite(lastDxPin, HIGH);
      break;

    case dxDec:
      digitalWrite(firstDxPin, LOW);
      digitalWrite(lastDxPin, HIGH);
      break;
  }
}

void Motor::stop(){
  switch (decode_pin) {
    case sxDec:
      digitalWrite(firstSxPin, LOW);
      digitalWrite(lastSxPin, LOW);
      break;

    case dxDec:
      digitalWrite(firstDxPin, LOW);
      digitalWrite(lastDxPin, LOW);
      break;
  }
}
