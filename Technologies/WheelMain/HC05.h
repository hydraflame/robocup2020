#ifndef HC05_h
#define HC05_h

#include <Arduino.h>
#include <SoftwareSerial.h>

const SoftwareSerial BTSerial(10, 11); // RX / TX

enum class Device{
  CV, WHEEL, BG
};

class HC05{
  public:
    void send(String s, Device dv);
    String read();
    HC05(char type);
    void wait();
  private:
    char type;
    String cvs = "#CV#";
    String wls = "#WL#";
    String bgs = "#BG#";
    String end = "#END#";
};

#endif
