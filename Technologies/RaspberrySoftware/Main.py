from imutils.video import VideoStream
from imutils.video import FPS
from imutils.object_detection import non_max_suppression
from threading import Thread
import numpy as np
import RPi.GPIO as gpio
import pytesseract
import argparse
import imutils
import time
import cv2

try:
    from PIL import Image
except ImportError:
    import Image

frame = None
schoolPin = 3
school = "SCUOLA"

def thread():
    while True:
        try:
            text = pytesseract.image_to_string(frame)
            print(text)
            if text == school:
                swapLevel()
            else:
                gpio.output(schoolPin, 0)
        except Exception as e:
            print(e)
            
def initPins():
    gpio.setmode(gpio.BOARD)
    gpio.setup(schoolPin, gpio.OUT, initial=0)
    gpio.output(schoolPin, 0)

def swapLevel():
    gpio.output(schoolPin, 1)
    
if __name__ == "__main__":
    
    print("[DEBUG] starting video stream...")
    vs = VideoStream(src=0).start()
    time.sleep(1.0)
    
    initPins()
    print("[DEBUG] Pins defined")
    fps = FPS().start()
    frame = vs.read()
    Thread(target = thread).start()
    
    while True:
        frame = vs.read()
        if frame is None:
            print("[INFO] Frame error, control if the camera is working")
            print("[INFO] Closing...")
            break

        orig = frame.copy()
        
        key = cv2.waitKey(1) & 0xFF
        if key == ord("q"): #press 'q' to close program
            print("[INFO] Closing...")
            gpio.output(schoolPin, 0)
            break

        fps.update()
        cv2.imshow("ROBOCUP", orig)
        time.sleep(0.1)

    cv2.destroyAllWindows()


