#ifndef Motor_h
#define Motor_h

class Motor{
  private:
    char decode_pin;
    int speed = 125;
  public:
    void init();
    void goForward();
    void goBack();
    void setVel();
    void stop();
    Motor(char dec);
};

#endif
