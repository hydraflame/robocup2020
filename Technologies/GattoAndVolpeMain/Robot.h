#ifndef Robot_h
#define Robot_h

#include "Motor.h"
#include "Defs.h"

struct Motors{
  Motor sxMotor = Motor(sxDec);
  Motor dxMotor = Motor(dxDec);

  void go(){
    //init all Motors
    sxMotor.init();
    dxMotor.init();
  }

  void goForward(){
    sxMotor.stop(); //before begin, stop all Motors
    dxMotor.stop();

    sxMotor.goForward(); //go forward all Motors
    dxMotor.goForward();
  }

  void goBack(){
    sxMotor.stop(); //before begin, stop all Motors
    dxMotor.stop();

    sxMotor.goBack(); //go back all Motors
    dxMotor.goBack();
  }

  void stop(){
    sxMotor.stop(); //stop all Motors
    dxMotor.stop();
  }
}

class Robot{
  private:
    Motors motors;
  public:
    void init();
    void forward();
    void back();
    void stop();
}
