# RoboCup 2020
Private repository of Zuccante's OnStage team HydraFlame
## Main technologies
- Deep Learning
- OpenCV
- Object Detection
## To-Do

* [ ]  Serial Data Communication between Arduino and Raspberry
* [ ]  Data Communication with Bluetooth module HC-06
* [ ]  Computer Vision with pytesseract
* [ ]  Computer Vision with personal libraries
* [ ]  Use of communication protocols
* [ ]  Laser distance sensor operation check
* [ ]  Hall effect sensor operation check
* [ ]  Management of json files for communication between Raspberry and PiCamera
